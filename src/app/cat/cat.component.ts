import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cat',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.css']
})
export class CatComponent implements OnInit {
  title:string = '바람이 되어~ ^ㅡ^;';
  cats:any[] = [];
  constructor() {
    for (var i=1;i<=10;i++){
      var cat = {name:'고양이'+i,age:i};
      this.cats.push(cat);
    }
  }
  clickCat(cat):void{
    alert('클릭하신 공양이의 이름은' + cat.name);
  }

  ngOnInit() {
  }
}
