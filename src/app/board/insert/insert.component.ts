import { Component, OnInit } from '@angular/core';
import { Board } from '../board';
import { ajax } from 'rxjs/ajax';
import { 
  HttpRequest,
  HttpClient,
  HttpEventType,
  HttpResponse
} from '@angular/common/http';


@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css']
})
export class InsertComponent implements OnInit {

  baseUrl:string ="http://localhost:88/";
  constructor(private _http:HttpClient) {}
  bi:Board = new Board();

  save(){
    // ajax.post("http://localhost:88/boardinfo",this.bi,
    // {'Content-Type':'application/json'}).subscribe(res=>{
    //   console.log(res);
    // });
    this.addBoardInfo(this.bi).subscribe(res=>{
      console.log(res);
    });
  }
  getFiles(evt){
    this.bi.biFile = evt.target.files[0];
  }

  ngOnInit() {
  }
  addBoardInfo(board:Board){
    const url = this.baseUrl + 'boardinfof';
    const formData = new FormData();
    for (var key in board) {
      formData.append(key,board[key]);
    }
    console.log(formData);
    const req = new HttpRequest('POST',url,formData);
    return this._http.request(req);
  }

}