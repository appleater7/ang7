import { Injectable } from '@angular/core';
import { JoinUser } from './join-user';
import { ajax } from 'rxjs/ajax';

@Injectable({
  providedIn: 'root'
})
export class JoinService {

  private baseUrl:string = 'http://localhost:88/';

  doJoin(ju:JoinUser){
    return ajax.post(this.baseUrl + 'join',{'ui_id':ju.ui_id,'ui_pwd':ju.ui_pwd,'ui_age':ju.ui_age});
  }
  constructor() { }
}
