import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { JoinUser } from './join-user';
import { JoinService } from './join.service';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css']
})
export class JoinComponent implements OnInit {
  ju:JoinUser = new JoinUser();

  join():void {
    this._js.doJoin(this.ju).subscribe(res=>{
      console.log(Response);
    });    
  }

  constructor(private _js:JoinService) { }

  ngOnInit() {
  }

}

